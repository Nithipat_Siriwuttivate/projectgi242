using System;

namespace DefaultNamespace
{
    public interface IDamagable
    {
        event Action OnExploded;
        void TakeHit(int damage);
        void Explode();
    }
}