using System;
using DefaultNamespace;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(audioSource != null,"audioSource cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void Explode()
        { 
            SoundManager.Instance.PlaySoundPlayerDeath();
            Debug.Assert(Hp <= 0, "Hp is more than zero ");
            Debug.Log("You are dead !!!");
            Destroy(gameObject);
            OnExploded?.Invoke();
            }

        private void OnCollisionEnter2D(Collision2D other)
        {
            Debug.Log("HIT");
        }
    }
}