﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace EnemyShip
{
    public class EnemyController1 : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship1 enemySpaceship1;
        private GameObject player;
        private void Start()
        {
            player = GameObject.FindWithTag("Player");
            InvokeRepeating("OnFire", 2, 0.5f);
        }
        private void Update()
        {
            MoveToPlayer();
        }
        
        private void OnFire()
        {
            enemySpaceship1.Fire();
        }

        private void MoveToPlayer()
        {
            var directionToPlayer = player.transform.position - transform.position;
            var velocity = directionToPlayer.normalized * enemySpaceship1.Speed;

            transform.Translate(new Vector3(velocity.x,0,0) * Time.deltaTime);
        }
    }    
}