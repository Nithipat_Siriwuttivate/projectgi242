using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        
        private GameManager gameManager;
        private int score;
        
        public void Init(GameManager gameManager) 
        { 
            this.gameManager = gameManager; 
            this.gameManager.OnRestarted += OnRestarted;
            score = 0;
        }
        
        public void AddScore()
        {
            score++;
        }
        private void Awake() 
        { 
            UIManager.Instance.HideScore(true);
        }
        
        private void OnRestarted() 
        { 
            gameManager.OnRestarted -= OnRestarted; 
            UIManager.Instance.HideScore(true);
            score = 0;
        }
        
        

        public int ShowScore()
        {
            return score;
        }
    }
}