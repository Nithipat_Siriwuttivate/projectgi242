using System;
using System.Net;
using DefaultNamespace;
using Spaceship;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemySpaceship1 enemySpaceship1;

        public event Action OnRestarted;
        private int enemyCount;
        private PlayerSpaceship spaceship;
        private EnemySpaceship enemy;
        private EnemySpaceship1 enemy1;
        private int score;

        private void Awake()
        {
            
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(enemySpaceship1 != null, "enemySpaceship1 cannot be null");

            enemyCount = 0;
        }
        
        public void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SpawnEnemySpaceship1();
            SoundManager.Instance.PlayBGM();
        }

        private void SpawnPlayerSpaceship()
        {
            spaceship = Instantiate(playerSpaceship);
            spaceship.Init(200, 50f);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Dead();
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            enemy = Instantiate(enemySpaceship);
            enemy.Init(150, 30f);
            enemy.OnExploded += OnEnemySpaceshipExploded;

            enemyCount++;
        }
        private void SpawnEnemySpaceship1()
        {
            enemy1 = Instantiate(enemySpaceship1);
            enemy1.Init(150, 40f);
            enemy1.OnExploded += OnEnemySpaceship1Exploded;

            enemyCount++;
        }
        

        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.AddScore();
            enemyCount--;
        }
        private void OnEnemySpaceship1Exploded()
        {
            ScoreManager.Instance.AddScore();
            enemyCount--;
        }
        
        
        
        public void Restart()
        {
            UIManager.Instance.SetDialogActive(true);
            OnRestarted?.Invoke();
        }

        

        private void Update()
        {
            if (enemyCount == 0 && UIManager.Instance.CheckActive() == false)
            {
                Win();
            }
        }

        private void Win()
        {
            spaceship.Remove();
            UIManager.Instance.End();
            UIManager.Instance.SetText($"Score : {ScoreManager.Instance.ShowScore()}");
            
        }
        
        public void Dead()
        {
            enemy.Remove();
            enemy1.Remove();
            UIManager.Instance.End();
            UIManager.Instance.SetText("You dead");
        }
    }
}