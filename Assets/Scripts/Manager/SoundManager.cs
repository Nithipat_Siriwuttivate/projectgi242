﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;
        
        public static SoundManager Instance { get; private set; }
        
        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
        }
        
        public enum Sound
        {
            BGM,
            Fire,
            EnemyFire,
            PlayerDead,
            EnemyDead,
        }
        
        /// <summary>
        /// Play sound
        /// </summary>
        /// <param name="audioSource"></param>
        /// <param name="sound"></param>
        public void Play(AudioSource audioSource, Sound sound)
        {
            Debug.Assert(audioSource != null, "audioSource cannot be null");

            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();
        }

        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.BGM);
        }

        public void PlaySoundEnemyDeath()
        {
            audioSource.loop = false;
            Play(audioSource, Sound.EnemyDead);
        }

        public void PlaySoundPlayerDeath()
        {
            audioSource.loop = false;
            Play(audioSource, Sound.PlayerDead);
        }

        public void Stop()
        {
            audioSource.Stop();
        }

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }
            }
            
            Debug.Assert(false,$"Cannot find sound {sound}");
            return null;
        }

        private void Awake()
        {
            Debug.Assert(audioSource != null, "audioSource cannot be null");
            Debug.Assert(soundClips != null, "soundClips cannot be null");

            if (Instance == null)
            {
                Instance = this;
            }
            
            DontDestroyOnLoad(this);
        }
    }
}
