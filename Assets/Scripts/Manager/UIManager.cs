﻿using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager: Singleton<UIManager>
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private TextMeshProUGUI scoreText;
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(text != null, "text cannot be null"); 
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(scoreText != null, "scoreText cannot be null");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);

            restartButton.gameObject.SetActive(false);
            text.gameObject.SetActive(false);
        }
        
        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            GameManager.Instance.StartGame();
        }

        private void OnRestartButtonClicked()
        {
            startButton.gameObject.SetActive(true);
            dialog.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);
            text.gameObject.SetActive(false);
            SoundManager.Instance.Stop();
            
            GameManager.Instance.Restart();
        }
        
        
        public void HideScore(bool hide) 
        { 
            scoreText.gameObject.SetActive(!hide); 
        }

        public void End()
        {
            startButton.gameObject.SetActive(false);
            dialog.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            scoreText.gameObject.SetActive(false);
        }

        public bool CheckActive()
        {
            return dialog.gameObject.activeInHierarchy;
        }

        public void SetText(string newText)
        {
            text.text = newText;
        }
        public void SetScoreText(string newText)
        {
            scoreText.text = newText;
        }
        
        public void SetDialogActive(bool show)
        {
            dialog.gameObject.SetActive(show);
        }
        
        
        
    }
}